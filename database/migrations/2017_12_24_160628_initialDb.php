<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    	Schema::create('categories', function(Blueprint $table){
    	    $table->increments('catid');
    	    $table->string('name');
    	    $table->string('image',1000);
    	    $table->string('description')->nullable();
    	    $table->timestamps();
    	});

    	Schema::create('images', function(Blueprint $table){
    		$table->increments('imid');
    		$table->integer('pid')->nullable();
    		$table->string('url',1000);
    		$table->timestamps();
    	});

        Schema::create('posts', function(Blueprint $table){
            $table->increments('pid');
            $table->integer('uid');
            $table->integer('catid');
            $table->string('content');
            $table->boolean('isKin')->default(0);
            $table->timestamps();
        });

        Schema::create('users', function(Blueprint $table){
            $table->increments('uid');
            $table->string('username');
            $table->string('name');
            $table->string('bio');
            $table->string('password');
            $table->string('email',191)->unique();
            $table->string('phone',191)->unique();
            $table->string('image',1000)->nullable();
            $table->timestamps();
        });

        Schema::create('user_following', function(Blueprint $table){
        	$table->increments('uffid');
        	$table->integer('uid');
        	$table->integer('following');
        	$table->timestamps();
        });


        Schema::create('messages', function(Blueprint $table){
            $table->increments('mid');
            $table->integer('sender');
            $table->integer('receiver');
            $table->string('content',5000);
            $table->boolean('fromSender');
            $table->boolean('isRead')->default(0);
            $table->timestamps();
        });

        Schema::create('events', function(Blueprint $table){
            $table->increments('eid');
	        $table->integer('sid');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('event_images', function(Blueprint $table){
            $table->increments('eiid');
            $table->integer('eid');
            $table->integer('imid');
            $table->timestamps();
        });

	    Schema::create( 'password_resets', function ( Blueprint $table ) {
		    $table->increments('prid');
		    $table->string( 'email' ,191)->index();
		    $table->string( 'token' );
		    $table->timestamp( 'created_at' )->nullable();
	    } );

	    Schema::create('brands', function(Blueprint $table){
	        $table->increments('bid');
	        $table->string('image',1000);
	        $table->string('name');
	        $table->string('description',5000);
	        $table->string('phone');
	        $table->string('email');
	        $table->string('instagram');
	        $table->string('facebook');
	        $table->string('twitter');
	        $table->timestamp('expiry');
	        $table->timestamps();
	    });

	    Schema::create('confirmations', function(Blueprint $table){
		    $table->increments('confid');
		    $table->string('phone')->nullable();
		    $table->string('email')->nullable();
		    $table->string('code');
		    $table->timestamps();
	    });


	    Schema::create( 'staff', function ( Blueprint $table ) {
		    $table->increments( 'sid' );
		    $table->string( 'name' );
		    $table->string( 'email',191 )->unique();
		    $table->string( 'password', 60 );
		    $table->rememberToken();
		    $table->softDeletes();
		    $table->timestamps();
	    } );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
