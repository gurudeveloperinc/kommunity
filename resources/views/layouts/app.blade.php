<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kommunity Portal</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="{{url('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{url('vendors/bower_components/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{url('vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css')}}">
    <link rel="stylesheet" href="{{url('vendors/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
    <link href="{{url('css/photoswipe.css')}}" rel="stylesheet">
    <link href="{{url('css/default-skin/default-skin.css')}}" rel="stylesheet">

    <script>
        window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
		]); ?>

        const baseUrl = '<?php echo url('/'); ?>'
    </script>

    <!-- App styles -->
    <link rel="stylesheet" href="{{url('css/app.min.css')}}">
    <script src="{{url('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{url('js/photoswipe.min.js')}}"></script>
    <script src="{{url('js/photoswipe-ui-default.min.js')}}"></script>

</head>

<body data-sa-theme="3">
<main class="main">
    <div class="page-loader">
        <div class="page-loader__spinner">
            <svg viewBox="25 25 50 50">
                <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>


    @include('header')
    @include('sidebar')

    <div class="themes">
        <div class="scrollbar-inner">
            <a href="" class="themes__item" data-sa-value="1"><img src="img/bg/1.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="2"><img src="img/bg/2.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="3"><img src="img/bg/3.jpg" alt=""></a>
            <a href="" class="themes__item active" data-sa-value="4"><img src="img/bg/4.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="5"><img src="img/bg/5.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="6"><img src="img/bg/6.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="7"><img src="img/bg/7.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="8"><img src="img/bg/8.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="9"><img src="img/bg/9.jpg" alt=""></a>
            <a href="" class="themes__item" data-sa-value="10"><img src="img/bg/10.jpg" alt=""></a>
        </div>
    </div>

    @yield("content")
</main>

<!-- Older IE warning message -->
<!--[if IE]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade to any of the following web browsers to access this website.</p>

        <div class="ie-warning__downloads">
            <a href="http://www.google.com/chrome">
                <img src="img/browsers/chrome.png" alt="">
            </a>

            <a href="https://www.mozilla.org/en-US/firefox/new">
                <img src="img/browsers/firefox.png" alt="">
            </a>

            <a href="http://www.opera.com">
                <img src="img/browsers/opera.png" alt="">
            </a>

            <a href="https://support.apple.com/downloads/safari">
                <img src="img/browsers/safari.png" alt="">
            </a>

            <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">
                <img src="img/browsers/edge.png" alt="">
            </a>

            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                <img src="img/browsers/ie.png" alt="">
            </a>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
<![endif]-->

<!-- Javascript -->
<!-- Vendors -->

<script src="{{url('vendors/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{url('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
<script src="{{url('vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js')}}"></script>

<script src="{{url('vendors/bower_components/salvattore/dist/salvattore.min.js')}}"></script>
{{--<script src="{{url('vendors/bower_components/flot/jquery.flot.js')}}"></script>--}}
{{--<script src="{{url('vendors/bower_components/flot/jquery.flot.resize.js')}}"></script>--}}
{{--<script src="{{url('vendors/bower_components/flot.curvedlines/curvedLines.js')}}"></script>--}}
<script src="{{url('vendors/bower_components/jqvmap/dist/jquery.vmap.min.js')}}"></script>
<script src="{{url('vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
<script src="{{url('vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<script src="{{url('vendors/bower_components/peity/jquery.peity.min.js')}}"></script>
<script src="{{url('vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('vendors/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>


<script src="{{url('vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{url('vendors/bower_components/jszip/dist/jszip.min.js')}}"></script>
<script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>


<!-- Charts and maps-->
{{--<script src="{{url('demo/js/flot-charts/curved-line.js')}}"></script>--}}
{{--<script src="{{url('demo/js/flot-charts/line.js')}}"></script>--}}
{{--<script src="{{url('demo/js/flot-charts/dynamic.js')}}"></script>--}}
{{--<script src="{{url('demo/js/flot-charts/chart-tooltips.js')}}"></script>--}}
{{--<script src="{{url('demo/js/other-charts.js')}}"></script>--}}
<script src="{{url('demo/js/jqvmap.js')}}"></script>


<!-- App functions and actions -->
<script src="{{url('js/app.min.js')}}"></script>
</body>
</html>