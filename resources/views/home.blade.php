<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')


    <section class="content">
        <header class="content__title">
            <h1>Dashboard</h1>
            <small>Welcome to the unique SuperAdmin web app experience!</small>

            <div class="actions">
                <a href="" class="actions__item zmdi zmdi-trending-up"></a>
                <a href="" class="actions__item zmdi zmdi-check-all"></a>

                <div class="dropdown actions__item">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="" class="dropdown-item">Refresh</a>
                        <a href="" class="dropdown-item">Manage Widgets</a>
                        <a href="" class="dropdown-item">Settings</a>
                    </div>
                </div>
            </div>
        </header>

        <div class="row quick-stats">
            <div class="col-sm-6 col-md-3">
                <a href="{{url('manage-users')}}">
                    <div class="quick-stats__item">
                        <div class="quick-stats__info">
                            <h2>{{count($users)}}</h2>
                            <small>Registered Users</small>
                        </div>
                        {{--<div class="quick-stats__chart peity-bar">6,4,8,6,5,6,7,8,3,5,9</div>--}}
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-md-3">
                <a href="{{url('manage-brands')}}">
                    <div class="quick-stats__item">
                        <div class="quick-stats__info">
                            <h2>{{count($brands)}}</h2>
                            <small>Brands</small>
                        </div>

                        {{--<div class="quick-stats__chart peity-bar">4,7,6,2,5,3,8,6,6,4,8</div>--}}
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-md-3">
                <a href="{{url('manage-events')}}">
                    <div class="quick-stats__item">
                        <div class="quick-stats__info">
                            <h2>{{count($events)}}</h2>
                            <small>Events</small>
                        </div>

                        {{--<div class="quick-stats__chart peity-bar">9,4,6,5,6,4,5,7,9,3,6</div>--}}
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-md-3">
                <a href="{{url('manage-categories')}}">
                    <div class="quick-stats__item">
                        <div class="quick-stats__info">
                            <h2>{{count($categories)}}</h2>
                            <small>Categories</small>
                        </div>

                        {{--<div class="quick-stats__chart peity-bar">5,6,3,9,7,5,4,6,5,6,4</div>--}}
                    </div>
                </a>
            </div>
        </div>

        <div class="row">
            {{--<div class="col-lg-6">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-body">--}}
                        {{--<h4 class="card-title">Recently Registered Brands</h4>--}}
                        {{--<h6 class="card-subtitle">Vestibulum purus quam scelerisque, mollis nonummy metus</h6>--}}

                        {{--<div class="flot-chart flot-curved-line"></div>--}}
                        {{--<div class="flot-chart-legends flot-chart-legends--curved"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Brands Expiring Soon</h4>
                        <h6 class="card-subtitle">Commodo luctus nisi erat porttitor ligula eget lacinia odio semnec</h6>
                        <div class="table-responsive">
                            <table id="data-table" class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Instagram</th>
                                    <th>Twitter</th>
                                    <th>Facebook</th>
                                    <th>Date Registered</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($brands as $brand)
                                    <tr>
                                        <td>{{$brand->name}}</td>
                                        <td>{{$brand->phone}}</td>
                                        <td>{{$brand->email}}</td>
                                        <td>{{$brand->instagram}}</td>
                                        <td>{{$brand->twitter}}</td>
                                        <td>{{$brand->facebook}}</td>
                                        <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$brand->created_at)->toDayDateTimeString()}}</td>
                                        <td>
                                            <a href="{{url('brand/' . $brand->bid )}}" class="btn btn-primary">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        @include('footer')
    </section>

@endsection