<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')

        <header class="content__title">
            <h1>CATEGORIES</h1>
        </header>

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">MANAGE CATEGORIES</h4>
                <h6 class="card-subtitle">There are {{count($categories)}} categories</h6>

                <div class="table-responsive">
                    <table id="data-table" class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Date Created</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                        <tr>
                            <td>{{$category->name}}</td>
                            <td>{{$category->description}}</td>
                            <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$category->created_at)->toDayDateTimeString()}}</td>
                            <td>
                                {{--<a href="{{url('edit-category')}}" class="btn btn-primary">Edit</a>--}}
                                <a href="{{url('delete-category/' . $category->catid)}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include('notification')
    </section>

    <script src="{{url('vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>


@endsection