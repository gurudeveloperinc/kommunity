<aside class="sidebar">
    <div class="scrollbar-inner">

        <div class="user">
            <div class="user__info" data-toggle="dropdown">
                <img class="user__img" src="demo/img/profile-pics/8.jpg" alt="">
                <div>
                    <div class="user__name">{{Auth::user()->name}}</div>
                    <div class="user__email">{{Auth::user()->email}}</div>
                </div>
            </div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{url('profile')}}">View Profile</a>
                {{--<a class="dropdown-item" href="">Settings</a>--}}
                <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
            </div>
        </div>

        <ul class="navigation">
            <li class="navigation__active"><a href="{{url('/')}}"><i class="zmdi zmdi-home"></i> Home</a></li>

            <li class="navigation__sub">
                <a href=""><i class="zmdi zmdi-format-underlined"></i> Users</a>

                <ul>
                    <li><a href="{{url('manage-users')}}">Manage</a></li>
                </ul>
            </li>

            <li class="navigation__sub">
                <a href=""><i class="zmdi zmdi-calendar"></i> Events</a>

                <ul>
                    <li><a href="{{url('add-event')}}">Add</a></li>
                    <li><a href="{{url('manage-events')}}">Manage</a></li>
                </ul>
            </li>

            <li class="navigation__sub">
                <a href=""><i class="zmdi zmdi-widgets"></i> Brands</a>

                <ul>
                    <li><a href="{{url('add-brand')}}">Add</a></li>
                    <li><a href="{{url('manage-brands')}}">Manage</a></li>
                </ul>
            </li>

            <li class="navigation__sub">
                <a href=""><i class="zmdi zmdi-group-work"></i> Categories</a>

                <ul>
                    <li><a href="{{url('add-category')}}">Add</a></li>
                    <li><a href="{{url('manage-categories')}}">Manage</a></li>
                </ul>
            </li>


        </ul>
    </div>
</aside>