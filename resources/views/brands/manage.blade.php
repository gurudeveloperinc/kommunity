<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')
        <header class="content__title">
            <h1>BRANDS</h1>
        </header>

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">MANAGE BRANDS</h4>
                <h6 class="card-subtitle">There are {{count($brands)}} brands</h6>

                <div class="table-responsive">
                    <table id="data-table" class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Instagram</th>
                            <th>Twitter</th>
                            <th>Facebook</th>
                            <th>Date Registered</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($brands as $brand)
                            <tr>
                                <td>{{$brand->name}}</td>
                                <td>{{$brand->phone}}</td>
                                <td>{{$brand->email}}</td>
                                <td>{{$brand->instagram}}</td>
                                <td>{{$brand->twitter}}</td>
                                <td>{{$brand->facebook}}</td>
                                <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$brand->created_at)->toDayDateTimeString()}}</td>
                                <td>
                                    <a href="{{url('brand/' . $brand->bid )}}" class="btn btn-primary">View</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include('footer')
    </section>

    <script src="{{url('vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>


@endsection