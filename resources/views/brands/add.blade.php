@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')
        <div class="content__inner">

            <div class="card">
                <div class="card-body">

                    <form method="post" enctype="multipart/form-data" action="{{url('add-brand')}}">

                        {{csrf_field()}}

                        <h4 class="card-title">Add Brand</h4>
                        <h6 class="card-subtitle">Here you can add brands that can be posted to by komunnity members.</h6>

                        <h3 class="card-body__title">Name</h3>

                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Name">
                            <i class="form-group__bar"></i>
                        </div>

                        <h3 class="card-body__title">Image</h3>

                        <div class="form-group">
                            <input type="file" name="image" class="form-control" placeholder="Image">
                            <i class="form-group__bar"></i>
                        </div>

                        <h3 class="card-body__title">Description</h3>

                        <div class="form-group">
                            <textarea class="form-control textarea-autosize" name="description" placeholder="Describe the category"></textarea>
                            <i class="form-group__bar"></i>
                        </div>

                        <h3 class="card-body__title">Email</h3>

                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Brand's email">
                            <i class="form-group__bar"></i>
                        </div>

                        <h3 class="card-body__title">Phone</h3>

                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" placeholder="Enter phone number">
                            <i class="form-group__bar"></i>
                        </div>

                        <h3 class="card-body__title">Instagram</h3>

                        <div class="form-group">
                            <input type="text" name="instagram" class="form-control" placeholder="Enter instagram handle">
                            <i class="form-group__bar"></i>
                        </div>

                        <h3 class="card-body__title">Twitter</h3>

                        <div class="form-group">
                            <input type="text" name="twitter" class="form-control" placeholder="Enter twitter handle">
                            <i class="form-group__bar"></i>
                        </div>

                        <h3 class="card-body__title">Facebook</h3>

                        <div class="form-group">
                            <input type="text" name="facebook" class="form-control" placeholder="Enter facebook profile link">
                            <i class="form-group__bar"></i>
                        </div>


                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>

        @include('footer')
    </section>

@endsection