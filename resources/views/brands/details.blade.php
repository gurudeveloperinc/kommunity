<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')
        <div class="content__inner">

            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">BRAND - {{$brand->name}} </h4>
                    <h6 class="card-subtitle"></h6>

                    <div class="card">
                        <div class="card-body">
                            Phone - {{$brand->phone}} <br>
                            Email - {{$brand->email}} <br>
                            Instagram - {{$brand->instagram}} <br>
                            Facebook - {{$brand->facebook}} <br>
                            Twitter - {{$brand->twitter}}
                            Expiry - {{Carbon::createFromFormat("Y-m-d H:i:s",$brand->expiry)->toDayDateTimeString()}}
                        </div>
                    </div>

                    <a href="{{url('manage-brands')}}" class="btn btn-warning">Back</a>

                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-subtitle">Extend Expiry</h6>

                            <form method="post" action="{{url('add-expiry')}}">
                                {{csrf_field()}}

                                <input type="hidden" name="bid" value="{{$brand->bid}}">

                                <label style="color: #000000; ">Days:</label>
                                <input name="days" type="text">

                                <button class="btn btn-dark" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        @include('footer')
    </section>



@endsection