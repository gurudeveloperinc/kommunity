<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')
        <div class="content__inner">

            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">EVENT - {{$event->name}} </h4>
                    <h6 class="card-subtitle">There are {{count($event->Images)}} images for this event</h6>

                    <div class="table-responsive">
                        <table id="data-table" class="table">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Images</th>
                                <th>Date Added</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php $count = 1; ?>
                            @foreach($event->Images as $image)
                                <tr>
                                    <td>N{{$count}}</td>
                                    <td>
                                        <img src="{{$image->url}}" class="img img-thumbnail" style="height: 100px; width:100px;">
                                    </td>
                                    <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$image->created_at)->toDayDateTimeString()}}</td>
                                    <td>
                                        <a href="{{url('delete-event-image/' . $image->imid)}}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
								<?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{url('manage-events')}}" class="btn btn-warning">Back</a>
                        <hr>
                        <br>
                        @foreach($event->Images as $image)
                            <img src="{{$image->url}}" style="height: 200px;" class="openGallery img img-thumbnail col-md-3 col-sm-6" >
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
        @include('footer')
    </section>



@endsection