<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <section class="content">
        <header class="content__title">
            <h1>USERS</h1>
        </header>

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">MANAGE USERS</h4>
                <h6 class="card-subtitle">There are {{count($users)}} users</h6>

                <div class="table-responsive">
                    <table id="data-table" class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Date Registered</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$user->created_at)->toDayDateTimeString()}}</td>
                                <td>
                                    <a href="{{url('user/' . $user->uid )}}" class="btn btn-primary">View</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include('notification')
    </section>

    <script src="{{url('vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>


@endsection