@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')
        <div class="content__inner">

            <div class="card">
                <div class="card-body">

                    <form method="post" action="{{url('add-event')}}" enctype="multipart/form-data">

                        {{csrf_field()}}
                        <input multiple onchange="readURL(this);" type="file" id="hiddenFile1" style="display: none" name="images[]">

                        <h4 class="card-title">Add Event</h4>
                        <h6 class="card-subtitle">Here you can add events to be viewed by kommunity members. Add only 10 photos max. You can add more when managing the event.</h6>

                        <h3 class="card-body__title">Name</h3>

                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Name">
                            <i class="form-group__bar"></i>
                        </div>

                        <div class="row">
                            <div id="preview" class="postImagePreview"></div>
                        </div>


                        <div class="form-group">
                            <a  id="addImageButton" class="btn btn-primary">Add Images</a>
                        </div>



                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>

        @include('footer')
    </section>

    <style>
        .postImagePreview .image {
            float: left;
        }
        .image img{
            width:200px;
            height: auto;
            margin: 10px;
        }
    </style>
    <script>

        $(document).ready(function () {

            $('#addImageButton').on('click',function(){
                $('#hiddenFile1').click();
            });


        });
        function hover(item){

            console.log('hovered');
            $(item).css('background-color','red');
            $(item).children('img').css('opacity',0.5);
            console.log($(item).children('img'));


        }

        function removeHover(item){
            console.log('cleared');
            $(item).css('background-color','transparent');
            $(item).children('img').css('opacity',1);
        }

        function removeItem(item){
            console.log('removed');
            $(item).remove();
        }

        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<div class=" image" onclick="removeItem(this)" onmouseleave="removeHover(this)"  onmouseover="hover(this)"> <img src ="' + e.target.result + '"></div>');
                    };


                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>
@endsection