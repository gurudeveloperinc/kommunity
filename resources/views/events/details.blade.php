<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')
        <div class="content__inner">

            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">EVENT - {{$event->name}} </h4>
                    <h6 class="card-subtitle">There are {{count($event->Images)}} images for this event</h6>

                    <div class="table-responsive">
                        <table id="data-table" class="table">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Images</th>
                                <th>Date Added</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($event->Images as $image)
                                <tr>
                                    <td>N{{$count}}</td>
                                    <td>
                                        <img src="{{$image->url}}" class="img img-thumbnail" style="height: 100px; width:100px;">
                                    </td>
                                    <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$image->created_at)->toDayDateTimeString()}}</td>
                                    <td>
                                        <a href="{{url('delete-event-image/' . $image->imid)}}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{url('manage-events')}}" class="btn btn-warning">Back</a>
                        <hr>
                        <br>
                        @foreach($event->Images as $image)
                            <img src="{{$image->url}}" style="height: 200px;" class="openGallery img img-thumbnail col-md-3 col-sm-6" >
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
        @include('footer')
    </section>


    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
             It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>

    <script>

        $(document).ready(function(){
            window.Laravel.data = '<?php echo $ajaxImages; ?>';
            console.log(window.Laravel.data);
            var imageItems = JSON.parse(window.Laravel.data);

            $('.carousel').carousel();

            console.log(imageItems);

            function openGallery(){


                var pswpElement = document.querySelectorAll('.pswp')[0];

                // build items array
                var items = imageItems;

                // define options (if needed)
                var options = {
                    // optionName: 'option value'
                    // for example:
                    index: 0 // start at first slide
                };

                // Initializes and opens PhotoSwipe
                var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();

            }


            $('.openGallery').on('click',openGallery);


        });

    </script>


@endsection