<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')

        <header class="content__title">
            <h1>EVENTS</h1>
        </header>

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">MANAGE EVENTS</h4>
                <h6 class="card-subtitle">There are {{count($events)}} events</h6>

                <div class="table-responsive">
                    <table id="data-table" class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Images</th>
                            <th>Date Created</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event)
                            <tr>
                                <td>{{$event->name}}</td>
                                <td>{{count($event->Images)}}</td>
                                <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$event->created_at)->toDayDateTimeString()}}</td>
                                <td>
                                    <a href="{{url('view-event/' . $event->eid)}}" class="btn btn-light">View</a>
                                    <a href="{{url('add-event-images/' . $event->eid)}}" class="btn btn-dark">Add Images</a>
                                    <a href="{{url('delete-event/' . $event->eid)}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include('footer')

    </section>

    <script src="{{url('vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>


@endsection