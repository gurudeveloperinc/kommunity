@extends('layouts.app')

@section('content')

    <section class="content">
        @include('notification')
        <div class="content__inner">

            <div class="card">
                <div class="card-body">

                    <form method="post" action="{{url('add-event-images')}}" enctype="multipart/form-data">

                        <input type="hidden" name="eid" value="{{$event->eid}}">
                        {{csrf_field()}}
                        <input multiple onchange="readURL(this);" type="file" id="hiddenFile1" style="display: none" name="images[]">

                        <h4 class="card-title">Event - {{$event->name}}</h4>
                        <h6 class="card-subtitle">This event already has {{count($event->Images)}} images</h6>

                        <div class="row">
                            <div id="preview" class="postImagePreview"></div>
                        </div>


                        <div class="form-group">
                            <a  id="addImageButton" class="btn btn-primary">Add Images</a>
                        </div>

                        <button type="submit" class="btn btn-success">Save</button>
                        <a href="{{url('view-event/' . $event->eid)}}" class="btn btn-warning" style="margin-left: 10px">Back</a>
                    </form>
                </div>
            </div>
        </div>

        @include('footer')
    </section>

    <style>
        .postImagePreview .image {
            float: left;
        }
        .image img{
            width:200px;
            height: auto;
            margin: 10px;
        }
    </style>
    <script>

        $(document).ready(function () {

            $('#addImageButton').on('click',function(){
                $('#hiddenFile1').click();
            });


        });


        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<div class=" image"> <img src ="' + e.target.result + '"></div>');
                    };


                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>
@endsection