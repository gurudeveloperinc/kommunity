<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('kin-posts','ApiController@kinPosts');
Route::post('kommunity-posts','ApiController@kommunityPosts');

Route::post('sign-up','ApiController@signUp');
Route::post('sign-in','ApiController@signIn');
Route::post('edit-profile','ApiController@editProfile');
Route::post('change-profile-photo','ApiController@changeProfilePhoto');

Route::post('post','ApiController@post');
Route::post('post-image','ApiController@postImage');
Route::post('post-search','ApiController@postSearch');
Route::post('delete-post','ApiController@deletePost');


Route::post('categories','ApiController@categories');
Route::post('category-posts','ApiController@categoryPosts');




Route::post('follow-user','ApiController@followUser');
Route::post('unfollow-user','ApiController@unfollowUser');

Route::post('profile','ApiController@profile');

Route::post('events','ApiController@events');


Route::post('search','ApiController@search');

Route::post('message-users','ApiController@messageUsers');
Route::post('send-message','ApiController@sendMessage');
Route::post('conversation','ApiController@conversation');
Route::post('set-message-read','ApiController@setMessageRead');
Route::post('delete-message','ApiController@deleteMessage');
Route::post('delete-conversation','ApiController@deleteConversation');




