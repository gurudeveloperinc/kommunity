<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/logout','HomeController@logout');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('add-category','HomeController@addCategory');
Route::get('manage-categories','HomeController@manageCategories');
Route::get('delete-category/{catid}','HomeController@deleteCategory');

Route::get('add-event','HomeController@addEvent');
Route::get('add-event-images/{eid}','HomeController@addEventImages');
Route::get('view-event/{eid}','HomeController@eventDetails');
Route::get('manage-events','HomeController@manageEvents');
Route::get('delete-event/{eid}','HomeController@deleteEvent');
Route::get('delete-event-image/{imid}','HomeController@deleteEventImage');


Route::get('add-brand','HomeController@addBrand');
Route::get('brand/{bid}','HomeController@brandDetails');
Route::get('manage-brands','HomeController@manageBrands');


Route::get('manage-users','HomeController@manageUsers');
Route::get('user/{uid}','HomeController@userDetails');

Route::post('add-brand','HomeController@postAddBrand');
Route::post('add-category','HomeController@postAddCategory');
Route::post('add-event','HomeController@postAddEvent');
Route::post('add-expiry','HomeController@postAddExpiry');
Route::post('add-event-images','HomeController@postAddEventImages');