<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $primaryKey = 'pid';
    protected $table = 'posts';

	public function User() {
		return $this->belongsTo(user::class,'uid','uid');
    }

	public function Images() {
		return $this->hasMany(image::class,'pid','pid');
    }

	public function Category() {
		return $this->belongsTo(category::class,'catid','catid');
    }
}
