<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    protected $primaryKey = 'eid';
    protected $table = 'events';

	public function EventImage() {
		return $this->hasMany(event_image::class,'eid','eid');
    }
	public function Images() {
		return $this->hasManyThrough(image::class,event_image::class,'eid','imid');
    }
}
