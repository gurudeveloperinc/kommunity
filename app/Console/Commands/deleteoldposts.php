<?php

namespace App\Console\Commands;

use App\image;
use App\post;
use Carbon\Carbon;
use Illuminate\Console\Command;

class deleteoldposts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes posts older than 24 hours on the system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$posts = post::all();
    	$count = 0;

    	foreach($posts as $post){
    		$timePosted = Carbon::createFromFormat("Y-m-d H:i:s",$post->created_at);
    		$currentTime = Carbon::now();

            if($timePosted->diffInHours($currentTime) > 24){
            	post::destroy($post->pid);

            	$images = image::where('pid',$post->pid)->get();
            	foreach($images as $image){

		            $urlArray = explode("/", $image->url);
		            $fileName = $urlArray[3]. "/" . $urlArray[4];
		            unlink($fileName);
		            $image->delete();
	            }

	            $count++;
            }
	    }

	    echo "Deleted $count posts";
    }
}
