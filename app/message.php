<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class message extends Model
{
    protected $primaryKey = 'mid';
    protected $table = 'messages';

	public function Sender() {
		return $this->belongsTo(user::class,'sender','uid');
    }

	public function Receiver() {
		return $this->belongsTo(user::class,'receiver','uid');
    }

}
