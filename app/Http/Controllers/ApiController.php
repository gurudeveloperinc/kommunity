<?php

namespace App\Http\Controllers;

use App\category;
use App\confirmation;
use App\event;
use App\image;
use App\Mail\confirmEmail;
use App\message;
use App\post;
use App\post_image;
use App\user;
use App\user_following;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ApiController extends Controller
{
	public function signUp(Request $request) {

		$customerEmailCount = user::where("email",$request->input('email'))->count();
		if($customerEmailCount > 0) return array('message' => "Email already exists");

		$customerPhoneCount = user::where("phone",$request->input('phone'))->count();
		if($customerPhoneCount > 0) return array('message' => "Phone already exists");

		try{

		$response = DB::transaction( function () {
			global $request;

			$user           = new user();
			$user->username = $request->input( 'username' );
			$user->name     = $request->input( 'name' );
			$user->email    = $request->input( 'email' );
			$user->phone    = $request->input( 'phone' );
			$user->bio      =  $request->input('bio');
			$user->image    = url( '/img/default-profile.png' );
			$user->password = bcrypt( $request->input( 'password' ) );
			$user->save();


			$code                = Str::random( '10' );
			$confirmation        = new confirmation();
			$confirmation->email = $user->email;
			$confirmation->code  = $code;
			$saved               = $confirmation->save();

//			if ( $saved ) {
//				Mail::to( $user->email )->send( new confirmEmail( $code, $user ) );
//			}

			return array( 'message' => $user->uid );
		},2);

		return $response;

		}catch (\Exception $exception){
			return response($exception,500);
		}
	}

	public function signIn( Request $request ) {
		$username = $request->input('email');
		$password = $request->input('password');

		$customer = user::where('email',$username)->first();

		if(count($customer) > 0) {
			if ( password_verify( $password, $customer->password ) ) {

				$kins = array();

				try{
					$customer->Posts;
					foreach($customer->Posts as $userPost)
						$userPost->Images;
				}catch (\Exception $exception){}

				foreach($customer->Following as $following){
					foreach($customer->Followers as $follower){
						if($following->uid  == $follower->uid)
							array_push($kins,$following);
					}
				}

				try{$customer->Followers;}catch (\Exception $exception){}

				$customer['kins']= $kins;

				return $customer;
			} else {
				return 0;
			}

		} else {
			return 0;
		}

	}

	public function editProfile( Request $request ) {

		try{
			$uid = $request->input('uid');
			$user = user::find($uid);
			$user->update($request->all());
			return 1;

		}catch (\Exception $exception){
			return response($exception,500);
		}

	}

	public function categoryPosts( Request $request ) {
		$catid = $request->input('catid');
		$category = category::find($catid);

		$category->Posts;

		foreach($category->Posts as $item){
			try{$item->Images;}catch (\Exception $exception){}
			try{$item->User;}catch (\Exception $exception){}
		}

		return $category;
	}

	public function postSearch( Request $request ) {
		$term = $request->input('term');
		$posts = post::where('content','like',"%#$term%")->get();

		foreach($posts as $item){
			$item->Images;
			$item->User;
		}

		return $posts;
	}

	public function changeProfilePhoto( Request $request ) {
		$image = $request->file('image');
		$uid = $request->input('uid');

		if(!isset($image)) return response(array("message" => "Image is required."),400);
		if(!isset($uid)) return response(array("message" => "UID is required."),400);

		$inputFileName = Carbon::now()->timestamp . Str::random(6) . $image->getClientOriginalName();
		$image->move("uploads",$inputFileName);

		$user = user::find($uid);
		$user->image = url('uploads/' . $inputFileName);
		$user->save();

		return response(array("message" => "Successful"));


	}

	public function kinPosts(Request $request) {
		$uid = $request->input('uid');

		$user = user::find($uid);

		$following = $user->Following;

		$followingArray = array($user->uid);
		foreach($following as $item){
			array_push($followingArray,$item->uid);
		}
		$posts = post::whereIn('uid',$followingArray)->get();

		foreach($posts as $post){
			try{$post->Images;}catch (\Exception $exception){}
			try{$post->User;}catch (\Exception $exception){}
			try{$post->Category;}catch (\Exception $exception){}
		}

		return array("message" => "Successful","data" => $posts);

	}

	public function events() {
		$events = event::all();


		foreach ($events as $event){
			$event->Images;
//			$event->EventImage;
//			foreach($event->EventImage as $item){
//				$item->Image;
//			}
		}
		return response(array("message" => "Successful", "data" => $events));
	}

	public function kommunityPosts() {
		$posts = post::where('isKin',0)->get();
		foreach($posts as $post) {
			try{$post->Images;}catch (\Exception $exception){}
			try{$post->User;}catch (\Exception $exception){}
			try{$post->Category;}catch (\Exception $exception){}
			try{$post->User->Posts;}catch (\Exception $exception){}

//			foreach($post->User->Posts as $userPost) get multiple images from a post
//			$userPost->Images;
		}

		return array("message" => "Successful","data" => $posts);
	}


	public function post( Request $request ) {

		try{
			$image = $request->file('image');
			$uid = $request->input('uid');
			$catid = $request->input('catid');
			$content = $request->input('content');

			if(!isset($image)) return response(array("message" => "Image is required."),400);
			if(!isset($uid)) return response(array("message" => "UID is required."),400);
			if(!isset($content)) return response(array("message" => "Content is required."),400);

			$post = new post();
			$post->uid = $uid;
			$post->catid = $request->input('catid');
			$post->content = $request->input('content');
			$post->isKin = $request->input('isKin');
			$post->save();

			$inputFileName = Carbon::now()->timestamp . Str::random(6) . $image->getClientOriginalName();
			$image->move("uploads",$inputFileName);

			$image = new image();
			$image->pid = $post->pid;
			$image->url = url('uploads/' . $inputFileName);
			$image->save();

			return array("message" => "Successful", "data" => $post);


		}catch (\Exception $exception){}

	}

	public function deletePost( Request $request ) {
		$uid = $request->input('uid');
		$pid = $request->input('pid');

		if(post::where('pid',$pid)->count() <= 0 ) return response(array("message" => "Post does not exist"),400);

		$post = post::find($pid);

		if($post->uid == $uid) {

			try{
				foreach($post->Images as $image){
					$urlArray = explode("/",$image->url);
					$url = $urlArray[count($urlArray) - 2] . "/" . $urlArray[count($urlArray) - 1];
					unlink($url);
				}
			}catch (\Exception $exception){}

			$post->delete();


			return response(array("message" => "Successful"));
		}

		else return response(array("message" => "Unauthorized."),406);

	}

	public function postImage( Request $request  ) {
		try{
			$pid = $request->input('pid');
			$image = $request->file('image');

			$inputFileName = Carbon::now()->timestamp . $image->getClientOriginalName();
			$image->move("uploads",$inputFileName);

			$image = new image();
			$image->pid = $pid;
			$image->url = url('uploads/' . $inputFileName);
			$status = $image->save();

			if($status) echo 1; else echo 0;

		} catch (\Exception $exception){
			return 0;
		}
	}

	public function categories() {
		try{
			$categories = category::all();
			return $categories;
		}catch (\Exception $exception){
			return 0;
		}
	}

	public function followUser( Request $request ) {
		$uid = $request->input('uid');
		$following = $request->input('following');

		if(user_following::where('uid',$uid)->where('following',$following)->count() > 0) return response(array("message" => "Already following."),400);

		$userFollowing = new user_following();
		$userFollowing->uid = $uid;
		$userFollowing->following = $following;
		$userFollowing->save();

		return array("message" => "Successful","data" => $userFollowing);
	}

	public function unfollowUser(Request $request) {
		$uid = $request->input('uid');
		$following = $request->input('following');

		if(user_following::where('uid',$uid)->where('following',$following)->count() <= 0) return response(array("message" => "Not following."),400);

		user_following::where('uid',$uid)->where('following',$following)->first()->delete();

		return array("message" => "Successful");
	}

	public function profile(Request $request) {

		$uid = $request->input('uid');
		if(!isset($uid)) return response(array("message" => "UID is required"),400);
		if(user::where('uid',$uid)->count() <=  0) return response(array("message" => "User does not exist"),400);

		$user = user::find($uid);

		try{$user->Following;}catch (\Exception $exception){}
		try{$user->Followers;}catch (\Exception $exception){}
		try{
			$user->Posts;
			foreach($user->Posts as $userPost)
				$userPost->Images;
		}catch (\Exception $exception){}

		$kins = array();

		foreach($user->Following as $following){
			foreach($user->Followers as $follower){
				if($following->uid  == $follower->uid)
					array_push($kins,$following);
			}
		}

		$user['kins']= $kins;

		return response(array("message" => "Successful", "data" => $user));

	}

	public function messageUsers( Request $request ) {

		$uid = $request->input('uid');
		$users = array();

		$messages = message::all()->where('receiver', $uid);
		foreach($messages as $item){
			array_push($users,$item->Sender);
		}

		$messages = message::all()->where('sender', $uid);
		foreach($messages as $item){
			array_push($users,$item->Receiver);
		}

		$users = collect($users)->unique()->flatten();

		foreach($users as $user){
			$lastMessage = message::where('sender',$user->uid)->orWhere('receiver',$user->uid)->get()->last();
			$unread = message::where('sender',$user->uid)->orWhere('receiver',$user->uid)->where('isRead',0)->count();
			$user['lastMessage'] = $lastMessage;
			$user['unread'] = $unread;
		}

		return $users;
	}

	public function sendMessage( Request $request ) {
		$sender = $request->input('sender');
		$receiver = $request->input('receiver');
		$content = $request->input('content');

		try{
			$message = new message();
			$message->sender = $sender;
			$message->receiver = $receiver;
			$message->content = $content;
			$message->save();

			return array("message" => "Successful", "data" => $message);

		}catch (\Exception $exception){

			return response(array("message" => "error"),500);
		}

	}

	public function conversation(Request $request) {
		$sender = $request->input('sender');
		$receiver = $request->input('receiver');
		$sentMessages = message::where('receiver',$receiver)->where('sender',$sender)->get();
		$receivedMessages = message::where('receiver',$sender)->where('sender',$receiver)->get();


		$messages = collect($sentMessages)->merge($receivedMessages)->sortBy('created_at',SORT_ASC)->flatten();
//		$messages = collect($sentMessages)->merge($receivedMessages)->flatten();

		foreach($messages as $message){
			if($message->sender == $sender)
				$message->fromSender = 1;
			else
				$message->fromSender = 0;
		}
		return $messages;
	}

	public function setMessageRead(Request $request) {
		$mid = $request->input('mid');

		if(message::where('mid', $mid)->count() <= 0) return response(array("message" => "Message does not exist"),400);

		$message = message::find($mid);
		if($message->isRead == 1) return response(array("message" => "Already read.","data" => $message),406);

		$message = message::find($mid);
		$message->isRead = 1;
		$message->save();
		return array("message" => "Successful","data" => $message);
	}

	public function deleteMessage( Request $request ) {
		$mid = $request->input('mid');

		if(message::where('mid', $mid)->count() <= 0) return response(array("message" => "Message does not exist"),400);

		message::destroy($mid);
		return array("message" => "Message Deleted.", "mid" => $mid);
	}

	public function deleteConversation( Request $request ) {
		$sender = $request->input('sender');
		$receiver = $request->input('receiver');
		$messages = message::where('receiver',$receiver)->where('sender',$sender)->get();

		foreach($messages as $message){
			message::destroy($message->mid);
		}

		return array("message" => "Conversation Deleted.");

	}


}
