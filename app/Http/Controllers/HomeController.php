<?php

namespace App\Http\Controllers;

use App\brand;
use App\category;
use App\event;
use App\event_image;
use App\image;
use App\user;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$brands = brand::all();
    	$events = event::all();
    	$categories = category::all();
    	$users = user::all();

        return view('home',[
        	'brands' => $brands,
	        'events' => $events,
	        'categories' => $categories,
	        'users' => $users
        ]);
    }

	public function addCategory() {
		return view('categories.add');
    }

	public function manageCategories() {
    	$categories = category::all()->sortByDesc('created_at');

		return view('categories.manage',[
			'categories' => $categories
		]);
    }


	public function deleteCategory( Request $request,  $catid ) {
		category::destroy($catid);
		$request->session()->flash('success','Category Deleted');
		return redirect('manage-categories');
    }

	public function addEvent() {
		return view('events.add');
    }

	public function deleteEvent( Request $request , $eid ) {
    	$event = event::find($eid);

    	foreach($event->Images as $item){


    		$imid = $item->imid;
		    $image = image::find($imid);
		    $urlArray = explode("/", $image->url);
		    $fileName = $urlArray[3]. "/" . $urlArray[4];
		    unlink($fileName);
		    $image->delete();
	    }

		$eventImages = event_image::where('eid',$eid)->get();

    	foreach($eventImages as $eventImage){
    		$eventImage->delete();
	    }

	    $event->delete();

		$request->session()->flash('success','Event Deleted.');
		return redirect('manage-events');
	}


	public function eventDetails( $eid ) {
		 $event = event::find($eid);

		$requiredImages = array();
		foreach($event->Images as $item){
			$image = ['src' => $item->url,'w' =>1200, 'h' =>900  ];
			array_push($requiredImages,$image);
		}

		$requiredImages = json_encode($requiredImages,JSON_UNESCAPED_SLASHES);

		return view('events.details',[
    		'event' => $event,
			'ajaxImages' => $requiredImages
	    ]);
    }

	public function addEventImages($eid) {
    	$event = event::find($eid);
    	return view('events.addImages',[
    		'event' => $event
	    ]);
    }

	public function deleteEventImage( Request $request , $eiid ) {
		$eventImage = event_image::find($eiid);
		$imid = $eventImage->imid;
		$eid = $eventImage->eid;
		$image = image::find($imid);
		$urlArray = explode("/", $image->url);
		$fileName = $urlArray[3]. "/" . $urlArray[4];
		unlink($fileName);
		event_image::destroy($eiid);
		$image->delete();
		$request->session()->flash('success','Image Deleted.');
		return redirect('view-event/' . $eid );
	}



	public function manageEvents() {
    	$events = event::all()->sortByDesc('created_at');
		return view('events.manage',[
			'events' => $events
		]);
    }


	public function manageUsers() {
		$users = user::all()->sortByDesc('created_at');

		return view('users.manage',[
			'users' => $users
		]);
    }

	public function userDetails( $uid ) {
		$user = user::find($uid);

		return view('users.details',[
			'user' => $user
		]);
    }

	public function addBrand() {
		return view('brands.add');
    }

	public function manageBrands() {
    	$brands = brand::all()->sortByDesc('created_at');
		return view('brands.manage',[
			'brands' => $brands
		]);
    }

	public function brandDetails($bid) {
		$brand = brand::find($bid);
		return view('brands.details',[
			'brand' => $brand
		]);
    }

	public function postAddBrand( Request $request ) {
		$file = $request->file('image');

		$rand          = Str::random( 5 );
		$inputFileName = $file->getClientOriginalName();
		$file->move( "userUploads", $rand . $inputFileName );
		$url = url( 'userUploads/' . $rand . $inputFileName );

		$brand = new brand();
		$brand->name = $request->input('name');
		$brand->image = $url;
		$brand->phone = $request->input('phone');
		$brand->email = $request->input('email');
		$brand->description = $request->input('description');
		$brand->instagram = $request->input('instagram');
		$brand->facebook = $request->input('facebook');
		$brand->twitter = $request->input('twitter');
		$brand->save();

		$request->session()->flash('success','Brand Added');

		return redirect('add-brand');
	}

	public function postAddEventImages( Request $request ) {
		$eid = $request->input('eid');
		$event = event::find($eid);

		if($request->hasFile('images')){
			foreach ( $request->file( 'images' ) as $item ) {
				$rand          = Str::random( 5 );
				$inputFileName = $item->getClientOriginalName();
				$item->move( "userUploads", $rand . $inputFileName );

				$image      = new image();
				$image->url = url( 'userUploads/' . $rand . $inputFileName );
				$image->save();

				$eventImage = new event_image();
				$eventImage->eid = $event->eid;
				$eventImage->imid = $image->imid;
				$eventImage->save();

			}

		}

		$request->session()->flash('success','Event Image Added.');
		return redirect('add-event-images/' . $eid);
    }

	public function postAddEvent( Request $request ) {
    	$event = new event();
    	$event->name = $request->input('name');
    	$event->sid = Auth::user()->sid;
    	$event->save();

    	if($request->hasFile('images')){
		    foreach ( $request->file( 'images' ) as $item ) {
			    $rand          = Str::random( 5 );
			    $inputFileName = $item->getClientOriginalName();
			    $item->move( "userUploads", $rand . $inputFileName );

			    $image      = new image();
			    $image->url = url( 'userUploads/' . $rand . $inputFileName );
			    $image->save();

			    $eventImage = new event_image();
			    $eventImage->eid = $event->eid;
			    $eventImage->imid = $image->imid;
			    $eventImage->save();

		    }

	    }

    	$request->session()->flash('success','Event Added.');
    	return redirect('add-event');
    }

	public function postAddCategory( Request $request ) {

		if($request->hasFile('image')){
			$file = $request->file('image');

			$rand          = Str::random( 5 );
			$inputFileName = $file->getClientOriginalName();
			$file->move( "userUploads", $rand . $inputFileName );
			$url = url( 'userUploads/' . $rand . $inputFileName );

		} else {
			$url = url('img/logo.jpg');
		}

		$category = new category();
		$category->name = $request->input('name');
		$category->image = $url;
		$category->description = $request->input('description');
		$category->save();

		$request->session()->flash('success','Category Added Successfully');
		return redirect('add-category');

	}

	public function postAddExpiry( Request $request ) {
		$bid = $request->input('bid');
		$days = $request->input('days');
    	$brand = brand::find($bid);

    	$brand->expiry = Carbon::now()->addDays($days);
    	$brand->save();

    	$request->session()->flash('success','Extension Successful');
    	return redirect('brand');

	}


	public function logout() {
    	Auth::logout();
		return redirect('/');
    }

}
