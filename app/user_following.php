<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_following extends Model
{
	protected $primaryKey = 'uffid';
	protected $table = 'user_following';
}
