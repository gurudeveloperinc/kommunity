<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    protected $primaryKey = 'uid';
    protected $table = 'users';
    protected $guarded = [];
    protected $hidden = ['password'];

	public function SentMessages() {
		return $this->hasMany(message::class,'uid','sender');
    }

	public function ReceivedMessages() {
		return $this->hasMany(message::class,'uid','receiver');
    }

	public function Posts() {
		return $this->hasMany(post::class,'uid','uid');
    }

	public function Followers() {
		return $this->belongsToMany(user::class,'user_following','following','uid');
    }

	public function Following() {
		return $this->belongsToMany(user::class,'user_following','uid','following');
	}


}
