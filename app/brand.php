<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brand extends Model
{
    protected $primaryKey = 'bid';
    protected $table = 'brands';
}
