<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event_image extends Model
{
    protected $primaryKey = 'eiid';
    protected $table = 'event_images';

	public function Image() {
		return $this->hasOne(image::class,'imid','imid');
    }
}
